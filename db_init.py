#! /usr/bin/env python3

from peewee import *
import datetime


MY_DB_FILE="SolvedPuzzle.db"
my_db = SqliteDatabase(MY_DB_FILE)

class BaseModel(Model):
    class Meta:
        database = my_db

class SolvedPuzzle(BaseModel):
    id    = PrimaryKeyField()
    date  = DateTimeField(default = datetime.datetime.now)
    alpha = CharField()

def Verbindung_DB():
    my_db.connect()

def Schliesse_DB():
    my_db.close()

if __name__ == '__main__':

    Verbindung_DB()

    if SolvedPuzzle.table_exists():
        SolvedPuzzle.drop_table()
    SolvedPuzzle.create_table() # merker alternativ: avs_db.create_tables([SolvedPuzzle])

