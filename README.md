# proof-of-work-webapp

The webserver creates a cryptographic puzzle for the webclient + a javascript-program
that can solve the puzzle.
If the client has solved the puzzle (proof of work), then the webserver will grant access to 
the protected resource.

## Dependencies

python3, Flask-Package ("pip3 install flask"), cryptography-package

## Run
First run

	./webserver.py

then open a browser with http://127.0.0.1:9000/

## screenshot:
![screenshot](screenshots/screenshot-1.jpg)

## Licence
GPL v3

