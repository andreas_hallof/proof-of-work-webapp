#! /usr/bin/env python3

import os, sys; sys.path.append(os.getcwd())
from flask import Flask, render_template, request, send_from_directory, redirect
from db_init import *
from datetime import datetime
import random

app = Flask(__name__)

@app.before_request
def before_request():
    Verbindung_DB()

@app.teardown_request
def teardown_request(exception):
    Schliesse_DB()

@app.route('/')
def entry_point_index():

    MyDebug="hi"
    # xxx: das ist kein kryptographisch sicherer RNG, nur PoC
    MyChallenge="{}".format(random.randint(2**32+1,2**40-1))
    MyDifficulty=4

    return render_template("index.html",
                           MyDebug=MyDebug,
                           Challenge=MyChallenge,
                           Difficulty=MyDifficulty
                          )


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/sha256.js')
def sha256_js():
    return render_template("sha256.js")


if __name__ == '__main__':

    MY_Port=9000

    if not os.path.exists(MY_DB_FILE):
        print("{} existiert nicht (db_init.py aufrufen).".format(MY_DB_FILE))
        sys.exit(1)
    
    # xxx im Produktivbetrieb natuerlich mit debug=False
    app.run( port=MY_Port, debug=True )

